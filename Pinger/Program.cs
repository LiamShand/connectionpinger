﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;
using System.Runtime.InteropServices;

namespace Pinger
{

    class SessionThread
    {
        private string pingUrl;
        private int milisecondInterval; //Milisecond interval

        public SessionThread(string url, int milisecondInterval)
        {
            this.pingUrl = url;
            this.milisecondInterval = milisecondInterval;
        }

        public void Session()
        {
            
            Ping netMon = new Ping();
            DateTime begin = DateTime.Now;
            bool previouslyFailed = false;
            int failCount = 0;
            DateTime startFailure = begin;
            IPStatus startStatus = IPStatus.Success;
            DateTime endFailure = begin;
            IPStatus endStatus = IPStatus.Success;
            string fileName = begin.ToString();
            fileName = fileName.Replace('\\', '-');
            fileName = fileName.Replace('/', '-');
            fileName = fileName.Replace(':', '-');
            string oldName = fileName;
            fileName += ".txt";
            //fileName = fileName.Replace('-', '_');  //why not use regex
            Console.WriteLine("Created: "+fileName);
            StreamWriter sw = new StreamWriter(fileName);
            sw.AutoFlush = true;
            sw.Write("Downtimes:\r\n");


            int pingCount = 0;
            bool running = true;
            while (running)
            {   
                try
                {
                    PingReply response;
                    DateTime now = DateTime.Now;
                    try
                    {
                        response = netMon.Send(pingUrl);

                        if (response.Status != IPStatus.Success)
                        {

                            if (previouslyFailed == false)
                            {
                                startFailure = now;
                                startStatus = response.Status;
                                //Writing early
                                //sw.Write(startFailure.ToString());

                                previouslyFailed = true;
                            }
                            endFailure = now;
                            endStatus = response.Status;
                            failCount++;
                        }
                        else
                        {
                            if (previouslyFailed == true)
                            {
                                previouslyFailed = false;
                                //now reconnected, 

                                if (failCount > 1)
                                {
                                    Console.WriteLine(startFailure.ToString()+" - "+endFailure.ToString());
                                    sw.Write(startFailure.ToString()+" - "+endFailure.ToString());
                                }
                                else
                                {
                                    Console.WriteLine(startFailure.ToString());
                                    sw.Write(startFailure.ToString());
                                }
                                sw.Write("\r\n");
                                sw.Flush();

                                failCount = 0;
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        //response.Status = IPStatus.DestinationUnreachable;

                        if (previouslyFailed == false)
                        {
                            startFailure = now;
                            startStatus = IPStatus.DestinationUnreachable;
                            previouslyFailed = true;
                        }
                        endFailure = now;
                        endStatus = IPStatus.DestinationUnreachable;
                        failCount++;
                        //Console.WriteLine("Massive Error Occurred");

                    }
                    


                    Thread.Sleep(milisecondInterval);

                }
                catch (ThreadInterruptedException e)
                {
                    // rename with ending timestamp appended
                    //let thread end (run until the end, which will trigger Thread.Join())
                    //Console.WriteLine("Logging Stopped");
                    running = false;
                    if (previouslyFailed == true)
                    {
                        if (failCount > 1)
                        {
                            sw.Write(" - " + endFailure.ToString());
                        }
                        sw.Write("\r\n");
                    }
                }
            }

            sw.Dispose();

            string end = DateTime.Now.ToString();
            end = end.Replace('\\', '-');
            end = end.Replace('/', '-');
            end = end.Replace(':', '-');
            //do not rename the file yet, test other features recently added
            string newFileName = (oldName + " to " + end + ".txt");
            File.Move(fileName, newFileName);
            Console.WriteLine("Renamed file to: "+ newFileName);


        }

    }

    class Program
    {
        static int currentIndex = 0;
        static int threadLimit = 1;
        static SessionThread[] stArr = new SessionThread[threadLimit];
        static Thread[] tArr = new Thread[threadLimit];
        static bool exitSystem = false;

        #region Trap application termination
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            Console.WriteLine("Exiting system due to external CTRL-C, or process kill, or shutdown");

            if (currentIndex > 0)
            {
                foreach (Thread a in tArr)
                {
                    a.Interrupt();
                }
            }
            Thread.Sleep(1000); //simulate some cleanup delay

            //allow main to run off
            exitSystem = true;

            //shutdown right away so there are no lingering threads
            Environment.Exit(-1);

            return true;
        }
        #endregion

        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////

        static void HelpMessage()
        {
            Console.WriteLine("--------------\n"+
                "Command List: \n"+
                "end          //Used for ending the current session/s\n"+
                "exit         //Used for ending current session/s and exit program\n"+
                "start        //Used to start a new logging session\n"+
                "--------------");
        }

        static void Main(string[] args)
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            bool running = true;
            HelpMessage();
            

            while (running || !exitSystem)
            {
                switch (Console.ReadLine())
                {
                    case "exit":
                        running = false;
                        exitSystem = true;
                        goto case "end";

                    case "end":
                        //end all threads
                        if (currentIndex > 0)
                        {
                            foreach(Thread a in tArr)
                            {
                                a.Interrupt();
                            }
                        }
                        stArr = new SessionThread[threadLimit];
                        tArr = new Thread[threadLimit];
                        currentIndex = 0;
                        break;

                    case "start":
                        if (currentIndex < threadLimit)
                        {
                            int interval = 0;
                            string url = "";
                            Console.Write("Specify Pinging Interval (Seconds): ");
                            if (int.TryParse(Console.ReadLine(), out interval))
                            {
                                interval = interval * 1000;
                                Console.Write("Specify Pinging Url: ");
                                url = Console.ReadLine();
                                if (url != "")
                                {
                                    stArr[currentIndex] = new SessionThread(url, interval);
                                    tArr[currentIndex] = new Thread(new ThreadStart(stArr[currentIndex].Session));

                                    tArr[currentIndex].Start();
                                    //Console.WriteLine("Logging Started");
                                    currentIndex++;

                                }
                                else
                                {
                                    Console.WriteLine("Invalid Interval Specified is Empty");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid Interval Specified");
                            }

                        } else
                        {
                            Console.WriteLine("Currently Reached Maximum Number of Allowed Threads");
                        }


                        break;

                    case "help":
                        HelpMessage();
                        break;
                }
            }




        }
    }
}
